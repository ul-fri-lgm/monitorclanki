package fri.lgm.android;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.View;

public class GameView extends View {
	private Paint mPaint;
	private Bitmap bmp;
	private float imageX, imageY;

	// Konstruktor razreda
	public GameView(Context context) {
		super(context);
		initGameView();
	}

	// Konstruktor razreda
	public GameView(Context context, AttributeSet attrs) {
		super(context, attrs);
		initGameView();
	}

	// Metoda, ki inicializira lokalne spremenljivke
    private final void initGameView() {
    	mPaint = new Paint();	// Ustvarimo novo instanco razreda Paint
    	bmp = BitmapFactory.decodeResource(getResources(), R.drawable.slika3);	// Ustvarimo novo instanco razreda Bitmap iz izbranega vira
    }
	
    // Metoda za izris
    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);	// Klic metode nadrazreda
        canvas.drawBitmap(bmp, imageX, imageY, mPaint);		//Izris slike (bmp) na izbrano lokacijo (imageX in imageY) s pomocjo pomoznega razreda (mPaint) 
    }

    // Metoda za dolocanje polozaja izrisa slike
    public void setPosition(float x, float y) {
    	imageX = x - bmp.getWidth()/2;	// x komponento nastavimo na sredino slike
    	imageY = y - bmp.getHeight()/2;	// y komponento nastavimo na sredino slike
    }
}
