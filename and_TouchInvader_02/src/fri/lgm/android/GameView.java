package fri.lgm.android;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.View;

public class GameView extends View {
	private Paint mPaint;
	private Bitmap invaderImage; //slika napadalca1
	private Bitmap invaderImage2; //slika napadalca2
	private Bitmap invaderImage3; //slika napadalca3
	private Bitmap invaderImage4; //slika napadalca4
	
	private Bitmap playerImage; //slika igral�eve ladje

	private int numInvaders = 4; //�tevilo napadalcev, ki jih bomo izrisali
	private float invaderOffsetX = 0f; //medsebojni razmik med napadalci
	private float invaderOffsetY = 130f; //odmik "galerije" napadalcev od zgornjega roba zaslona
	
	private float playerOffsetY = 0f; //odmik igral�eve ladjice od spodnjega dela zaslona
	
	private float invaderPositionX = 0f; //pozicija najbolj levega napadalca, ki ga prikazujemo
	private float invaderPrevPositionX = 0f; //pozicija najbolj levega napadalca ob prej�njem dogodku dotika zaslonoa
	
	private float playerPositionX = 0f; //pozicija igral�eve ladje
	private float playerPrevPositionX = 0f; //pozicija igral�eve ladje ob prej�njem dogodku dotika zaslonoa
	

	// Konstruktor razreda
	public GameView(Context context) {
		super(context);
		initGameView();
	}

	// Konstruktor razreda
	public GameView(Context context, AttributeSet attrs) {
		super(context, attrs);
		initGameView();
	}

	// Metoda, ki inicializira lokalne spremenljivke
    private final void initGameView() {
    	mPaint = new Paint();	// Ustvarimo novo instanco razreda Paint
    	invaderImage = BitmapFactory.decodeResource(getResources(), R.drawable.invader1);
    	invaderImage2 = BitmapFactory.decodeResource(getResources(), R.drawable.invader2);
    	invaderImage3 = BitmapFactory.decodeResource(getResources(), R.drawable.invader3);
    	invaderImage4 = BitmapFactory.decodeResource(getResources(), R.drawable.invader4);
    	
        playerImage = BitmapFactory.decodeResource(getResources(), R.drawable.ship);
    }
	
    // Metoda za izris
    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);	// Klic metode nadrazreda
        
        invaderOffsetX = (this.getWidth() - numInvaders*invaderImage.getWidth()) / numInvaders;
        
        if(invaderPositionX < 0) invaderPositionX += this.getWidth();
        
        for(int i = 0; i < numInvaders; i++) {
        	canvas.drawBitmap(invaderImage, (invaderPositionX + i*(invaderOffsetX + invaderImage.getWidth())) % this.getWidth(), invaderOffsetY*0, mPaint);
            canvas.drawBitmap(invaderImage, (invaderPositionX - this.getWidth() + i*(invaderOffsetX + invaderImage.getWidth())) % this.getWidth(), invaderOffsetY*0, mPaint);
        }
        
        for(int i = 0; i < numInvaders; i++) {
        	canvas.drawBitmap(invaderImage2, (invaderPositionX + i*(invaderOffsetX + invaderImage.getWidth())) % this.getWidth(), invaderOffsetY*1, mPaint);
            canvas.drawBitmap(invaderImage2, (invaderPositionX - this.getWidth() + i*(invaderOffsetX + invaderImage.getWidth())) % this.getWidth(), invaderOffsetY*1, mPaint);
        }
        
        for(int i = 0; i < numInvaders; i++) {
        	canvas.drawBitmap(invaderImage3, (invaderPositionX + i*(invaderOffsetX + invaderImage.getWidth())) % this.getWidth(), invaderOffsetY*2, mPaint);
            canvas.drawBitmap(invaderImage3, (invaderPositionX - this.getWidth() + i*(invaderOffsetX + invaderImage.getWidth())) % this.getWidth(), invaderOffsetY*2, mPaint);
        }
        
        for(int i = 0; i < numInvaders; i++) {
        	canvas.drawBitmap(invaderImage4, (invaderPositionX + i*(invaderOffsetX + invaderImage.getWidth())) % this.getWidth(), invaderOffsetY*3, mPaint);
            canvas.drawBitmap(invaderImage4, (invaderPositionX - this.getWidth() + i*(invaderOffsetX + invaderImage.getWidth())) % this.getWidth(), invaderOffsetY*3, mPaint);
        }
        
        invaderPositionX = invaderPositionX % this.getWidth();
        
        if(playerPositionX < 0) playerPositionX += this.getWidth();
        
        playerOffsetY = this.getHeight()-200;
        
        canvas.drawBitmap(playerImage, playerPositionX, playerOffsetY, mPaint);
        canvas.drawBitmap(playerImage, playerPositionX - this.getWidth(), playerOffsetY, mPaint);
        
        playerPositionX = playerPositionX % this.getWidth();
        
    }

    // Metoda za dolocanje polozaja izrisa slike
    public void setPosition(float x, float y, boolean saveOnly) {
    	
    	if(y > this.getHeight()/2) { //preverimo Y koordinato, da ugotovimo, ali zelimo premakniti igralcevo ladjico ali sovraznika
    		if(saveOnly) {
        		playerPrevPositionX = x;
        		return;
        	}
        	float delta = x-playerPrevPositionX;
        	
        	playerPositionX = playerPositionX +  delta;
        	 
        	playerPrevPositionX = x;
    	} else {
    		if(saveOnly) {
        		invaderPrevPositionX = x;
        		return;
        	}
        	float delta = x-invaderPrevPositionX;
        	
        	invaderPositionX = invaderPositionX +  delta;
        	 
        	invaderPrevPositionX = x;	
    	}
    	
    	
    }
    
    public void destoryInvader(float x, float y) {
    	if(numInvaders > 1) {
    		if(x > playerPositionX && x < playerPositionX + playerImage.getWidth()) {
    			if(y > playerOffsetY && x < playerOffsetY + playerImage.getHeight()) {
    				numInvaders--;
    			}
    		}
    	}
    }
    
}
