package fri.lgm.android;

import android.app.Activity;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;

public class And_TouchInvaderActivity extends Activity {

	// Privatna realizacija poslusalca
    private OnTouchListener touchListener = new OnTouchListener() {
		
		@Override
		public boolean onTouch(View v, MotionEvent event) {
			int action=event.getAction();
			switch (action) {
				case MotionEvent.ACTION_DOWN:
					((GameView)v).setPosition(event.getX(), event.getY(), true);
					v.postInvalidate();
				case MotionEvent.ACTION_MOVE:
					((GameView)v).setPosition(event.getX(), event.getY(), false);
					v.postInvalidate();
					break;
				case MotionEvent.ACTION_UP:
					((GameView)v).destoryInvader(event.getX(), event.getY());
					break;
			}
			
			return true;
		}
	};
	
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);		// Uporaba razporejevalnika GUI-ja definiranega v main.xml

        GameView myView = (GameView)findViewById(R.id.gameView1);	// Pridobimo referenco na pogled GameView, ki smo ga dodali v vmesnik
        
        myView.setFocusable(true);	// Omogocimo pridobivanje fokusa
        myView.setFocusableInTouchMode(true);	// Omogocimo pridobivanje fokusa v primeru dotika
        myView.requestFocus();	// Zahtevamo fokus
    	
        myView.setOnTouchListener(touchListener);	// Dodamo poslusalca dotika na nas pogled
    }
}