package fri.lgm.android.CameraFilter;

import android.content.Context;
import android.hardware.Camera;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.ViewGroup;

class Predogled extends ViewGroup implements SurfaceHolder.Callback {
    
    Camera backCamera;
 
    Predogled(Context context, SurfaceView sv) {
        super(context);
        sv.getHolder().addCallback(this);
    }

    public void setCamera(Camera camera) {
    	backCamera = camera;
    	if (backCamera != null)
    		requestLayout();
    	
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
    	setMeasuredDimension(getSuggestedMinimumWidth(), getSuggestedMinimumHeight());
    }

    @Override
    protected void onLayout(boolean changed, int l, int t, int r, int b) {
    }

    public void surfaceCreated(SurfaceHolder holder) {
        
        try {
            if (backCamera != null) {
                backCamera.setPreviewDisplay(holder);
            }
        } catch (Exception e) { }
    }

    public void surfaceDestroyed(SurfaceHolder holder) {
        if (backCamera != null) {
            backCamera.stopPreview();
        }
    }

    public void surfaceChanged(SurfaceHolder holder, int format, int w, int h) {
    	System.out.println("SETTING" + " " + format + " " + w + " " + h);
    	if(backCamera != null) {
    		requestLayout();
    		backCamera.startPreview();
    	}
    }

}