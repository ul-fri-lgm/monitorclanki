package fri.lgm.android.CameraFilter;

import java.io.FileOutputStream;

import fri.lgm.android.R;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BitmapFactory.Options;
import android.graphics.Color;
import android.hardware.Camera;
import android.hardware.Camera.AutoFocusCallback;
import android.hardware.Camera.PictureCallback;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.SurfaceView;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.view.ViewGroup.LayoutParams;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;

public class CameraFilterActivity extends Activity {
	Predogled predogled;
	Button buttonClick;
	Camera camera;
	String fileName;
	Activity act;
	Context ctx;
	Handler handler;
	Context context;
	CameraFilterActivity activity = this;
	
	public Dialog usernamedialog;
	public Dialog commentdialog;

	@Override
	public void onStop() 
	{
		if(camera != null)
			camera.release();
		super.onStop();
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
	    inflater.inflate(R.menu.menu, menu);
		return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		Toast.makeText(context, "Menuitem " + item, Toast.LENGTH_SHORT);
		System.out.println("ADDING " + item.toString().toLowerCase());
		if(item.toString().toLowerCase().startsWith("uredi uporabnika")) {
	        usernamedialog = new Dialog(this);
	
	        usernamedialog.setContentView(R.layout.userdialog);
	        usernamedialog.setTitle("Uredi uporabnika");
	
	        usernamedialog.setCancelable(true);
	        Button b = (Button)usernamedialog.findViewById(R.id.confirmuser);
	        
	        SharedPreferences prefs = this.getSharedPreferences("CameraFilter",  MODE_PRIVATE);
	        
			EditText tv = (EditText)usernamedialog.findViewById(R.id.username);
			
	        tv.setText(prefs.getString("username", "noname"));
	
	        b.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					TextView tv = (TextView)((View)v.getParent()).findViewById(R.id.username);
					String s = tv.getText().toString();
					SharedPreferences prefs = getSharedPreferences("CameraFilter",  MODE_PRIVATE);
					SharedPreferences.Editor editor = prefs.edit();
					editor.putString("username", s);
					editor.commit();
					usernamedialog.cancel();
					
				}
			});
	        usernamedialog.show();
		} else if(item.toString().toLowerCase().startsWith("dodaj meta")) {
			commentdialog = new Dialog(this);
			
			commentdialog.setContentView(R.layout.commentdialog);
			commentdialog.setTitle("Uredi komentar");
	
			commentdialog.setCancelable(true);
	        Button b = (Button)commentdialog.findViewById(R.id.confirmcomment);
	        
	        SharedPreferences prefs = this.getSharedPreferences("CameraFilter",  MODE_PRIVATE);
	        
			EditText tv = (EditText)commentdialog.findViewById(R.id.comment);
			
	        tv.setText(prefs.getString("comment", ""));
	
	        b.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					TextView tv = (TextView)((View)v.getParent()).findViewById(R.id.comment);
					String s = tv.getText().toString();
					SharedPreferences prefs = getSharedPreferences("CameraFilter",  MODE_PRIVATE);
					SharedPreferences.Editor editor = prefs.edit();
					editor.putString("comment", s);
					editor.commit();
					commentdialog.cancel();
					
				}
			});
	        commentdialog.show();
		} else if(item.toString().toLowerCase().startsWith("o aplikaciji")) {
			Dialog aboutdialog = new Dialog(this);
			aboutdialog.setContentView(R.layout.aboutdialog);
			aboutdialog.setTitle("O aplikaciji / Pomo�");
	
			aboutdialog.setCancelable(true);
			aboutdialog.show();
		} else {
			camera.takePicture(null, null, null, jpegCallback);
		}
	    return true;
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		ctx = this;
		act = this;
		//requestWindowFeature(Window.FEATURE_NO_TITLE);
		//getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
		
		setContentView(R.layout.main);
		handler = new Handler(Looper.getMainLooper());
		context = getApplicationContext();
		
		predogled = new Predogled(this, (SurfaceView)findViewById(R.id.surfaceView));
		predogled.setLayoutParams(new LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.FILL_PARENT));
		((FrameLayout) findViewById(R.id.preview)).addView(predogled);
		predogled.setKeepScreenOn(true);

		buttonClick = (Button) findViewById(R.id.buttonClick);

		buttonClick.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				camera.takePicture(null, null, null, jpegCallback);
			}
		});
		
		buttonClick.setOnLongClickListener(new OnLongClickListener(){
			@Override
			public boolean onLongClick(View arg0) {
				camera.autoFocus(new AutoFocusCallback(){
					@Override
					public void onAutoFocus(boolean arg0, Camera arg1) {
						
					}
				});
				return true;
			}
		});
		
	}

	
	@Override
	protected void onResume() {
		super.onResume();
		camera = Camera.open();
		camera.startPreview();
		predogled.setCamera(camera);
		
	}

	@Override
	protected void onPause() {
		if(camera != null) {
			camera.stopPreview();
			predogled.setCamera(null);
			camera.release();
			camera = null;
		}
		super.onPause();
	}

	private void resetCam() {
		camera.startPreview();
		predogled.setCamera(camera);
	}

	public void postBackMessage(String message) {
		PostMessage pm = new PostMessage();
		pm.context = context;
		pm.message = message;
		handler.post(pm);
	}
	
	PictureCallback jpegCallback = new PictureCallback() {
		public void onPictureTaken(byte[] data, Camera camera) {
			
			long time = System.currentTimeMillis();
			
			Options o = new Options();
			o.inMutable = true;
			
			o.inSampleSize = 4;
			Bitmap bmp = BitmapFactory.decodeByteArray(data, 0, data.length, o);
			Bitmap bmp2 = BitmapFactory.decodeByteArray(data, 0, data.length, o);
			int width = bmp.getWidth();
			int height = bmp.getHeight();
			
			int[] tabela = new int[] { Color.GRAY, Color.GREEN, Color.RED, Color.BLUE };
			for(int x = 0; x < 1/*tabela.length*/; x++) {
				for(int i = 0; i < width; i++) {
					for(int j = 0; j < height; j++) {
						int pixel = bmp.getPixel(i, j);
						int red = (pixel >> 16) & 0xFF;
						int green = (pixel >> 8) & 0xFF;
						int blue = pixel & 0xFF;
						int grayvalue = red + green + blue;
						grayvalue /= 3;
						
						if(grayvalue >= 160) 
						{
							bmp2.setPixel(i, j, Color.WHITE);
						} else if(grayvalue >= 80) {
							bmp2.setPixel(i, j, tabela[x]);
						}
						else
						{
							bmp2.setPixel(i, j, Color.BLACK);
						}
					}
					
				}
				
		        FileOutputStream outStream = null;
				try {
					//odkomentiraj, �e �eli� shranjevat na disk
					//outStream = new FileOutputStream("/sdcard/DCIM/" + time + "_" + x + ".jpg");
					//bmp2.compress(Bitmap.CompressFormat.JPEG, 50, outStream);
					//outStream.close();	
					
					UploadThread ut = new UploadThread();
					ut.bmp = bmp2;
					LocationManager locationManager=(LocationManager) getSystemService(android.content.Context.LOCATION_SERVICE);
					ut.locationManager = locationManager;
					SharedPreferences prefs = getSharedPreferences("CameraFilter", MODE_PRIVATE);
					ut.username = prefs.getString("username", "noname");
					ut.parent = activity;
					ut.comment = prefs.getString("comment", "<i>brez komentarja</i>");
					ut.start();
					
					
				} catch (Exception e) { }
			}
			resetCam();
			
		}
	};
	
}
