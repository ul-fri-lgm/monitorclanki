package fri.lgm.android.CameraFilter;

import android.content.Context;
import android.widget.Toast;

public class PostMessage implements Runnable {

	public Context context;
	public String message;
	@Override
	public void run() {
		Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
		
	}

}
