package fri.lgm.android.CameraFilter;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import android.graphics.Bitmap;
import android.location.Location;
import android.location.LocationManager;
import android.os.Handler;
import android.util.Log;

public class UploadThread extends Thread {
		public Bitmap bmp = null; 
		public LocationManager locationManager = null; 
		public String username;
		public String comment = "";
		InputStream is;
		protected CameraFilterActivity parent;
		
		public void run() {
//			Bitmap from resources
			//Bitmap bitmapOrg = BitmapFactory.decodeResource(getResources(), R.drawable.a1);
			ByteArrayOutputStream bao = new ByteArrayOutputStream();
			bmp.compress(Bitmap.CompressFormat.JPEG, 75, bao);
			
//			Lokacija iz providerja
			double lat = 0;
    		double lon = 0;
    		
	    	List<String> providers=locationManager.getProviders(true);
	    	if (providers.size()>0) {
	    		Location location=locationManager.getLastKnownLocation(providers.get(0));
	    		lat=location.getLatitude();
	    		lon=location.getLongitude();
	    	} else {
	    		lat = Double.MIN_VALUE;
	    		lon = Double.MIN_VALUE;
	    	}
			
//	    	Kodiranje slike
			byte [] ba = bao.toByteArray();
			String ba1= fri.lgm.tmp.Base64.encodeBytes(ba);
			Log.d("Upload", ba1);
			ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
			nameValuePairs.add(new BasicNameValuePair("image",ba1));
			nameValuePairs.add(new BasicNameValuePair("gpsX", ""+lat));
			nameValuePairs.add(new BasicNameValuePair("gpsY", ""+lon));
			nameValuePairs.add(new BasicNameValuePair("comment", comment));
			nameValuePairs.add(new BasicNameValuePair("user", username));
			
			try{
				HttpClient httpclient = new DefaultHttpClient();
				HttpPost httppost = new HttpPost("http://atlas.fri.uni-lj.si:80/~ciril/androidwiki/kajdogaja/service.php");
//				HttpPost httppost = new HttpPost("http://88.200.89.7/~cirilbohak/service.php");
//				HttpPost httppost = new HttpPost("http://192.168.2.100/~cirilbohak/service.php");
				httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
				HttpResponse response = httpclient.execute(httppost);
				HttpEntity entity = response.getEntity();
	
				is = entity.getContent();
				Scanner sc = new Scanner(is);
//				TextView tv = (TextView) findViewById(R.id.textView1);
//				tv.setText(""+is);

				while (sc.hasNextLine()) {
					String s = sc.nextLine();
					Log.d("Upload", s);
					parent.postBackMessage(s);
				}
				
			}catch(Exception e){
				Log.e("Upload", "Error in http connection "+e.toString());
			}
		}
}
