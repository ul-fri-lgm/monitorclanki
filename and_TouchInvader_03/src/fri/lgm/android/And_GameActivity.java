package fri.lgm.android;

import android.app.Activity;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.text.style.MetricAffectingSpan;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;
import android.view.WindowManager.LayoutParams;
import android.widget.Toast;

public class And_GameActivity extends Activity {
	Game game;
	
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        
        setContentView(R.layout.main);		// Uporaba razporejevalnika GUI-ja definiranega v main.xml
                
        getWindow().addFlags(LayoutParams.FLAG_KEEP_SCREEN_ON);	// preprecimo zaklepanje in ugasanje zaslona
        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);
        System.out.println("density: " + metrics.density);
        Log.d("tralala", "density: " + metrics.density);
        Context context = getApplicationContext();
		CharSequence text = "Your screen's density: " + metrics.density;
		int duration = Toast.LENGTH_SHORT;

		Toast toast = Toast.makeText(context, text, duration);
		toast.show();
        game = new Game((GameView)findViewById(R.id.gameView1), metrics.heightPixels, metrics.widthPixels, metrics.density);	// Kreiramo novo igro in ji pripnemo GameView
        
    }
}