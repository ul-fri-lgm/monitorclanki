package fri.lgm.android;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BitmapFactory.Options;
import android.graphics.Canvas;
import android.graphics.RectF;
import android.renderscript.Sampler;

public class Bullet extends SpaceShip {
	Bitmap bulletImage;
	Bitmap bulletImage1;
	float xPos, yPos;
	int direction = 1;
	float speed = 15.0f;
	float windowHeight;
	int type = 0; 	// 0 - nas izstrelek, 1 - izstrelek napadalca
	int sampleSize = 1;
	
	public Bullet(Resources res, float windowHeight, int sampleSize) {
		super(res, sampleSize);
		Options o = new Options();
		o.inSampleSize = sampleSize;
		this.sampleSize = sampleSize;
		bulletImage = BitmapFactory.decodeResource(viri, R.drawable.bullet, o);
		bulletImage1 = BitmapFactory.decodeResource(viri, R.drawable.bullet, o);
		this.windowHeight = windowHeight;
	}

	public void render(Canvas c) {
		c.drawBitmap(bulletImage, null, new RectF(xPos - bulletImage.getWidth()/4/sampleSize, yPos - bulletImage.getHeight()/4/sampleSize, xPos + bulletImage.getWidth()/4/sampleSize, yPos + bulletImage.getHeight()/4/sampleSize), mPaint);
		Bitmap tmp = bulletImage;
		bulletImage = bulletImage1;
		bulletImage1 = tmp;
	}
	
	public void setPosition(float x, float y) {
		xPos = x;
		yPos = y;
	}
	
	public void setSpeed(float speed) {
		this.speed = speed;
	}
	
	public void move() {
		int factor = -1;
		//Log.d("HROSC", "trenutni y:" + yPos);
		if(type == 1) { //�e je izstrelek napadalca
			factor = 1;
		}
		yPos += factor * speed * direction;
	}
	
	public void setType(int type) {
		this.type = type;
		Options o = new Options();
		o.inSampleSize = sampleSize;
		switch (type) {
			case 0:
				image = bulletImage = bulletImage1 = BitmapFactory.decodeResource(viri, R.drawable.bullet, o);
				break;
			case 1:
				image = bulletImage = BitmapFactory.decodeResource(viri, R.drawable.bullet1, o);
				bulletImage1 = BitmapFactory.decodeResource(viri, R.drawable.bullet1a, o);
				break;
		}
	}
	
	public boolean isValid() {
		if (type == 0 && yPos > 10)
			return true;
		else if(type == 1 && yPos < windowHeight) 
			return true;
			
		return false;
	}

	
	public float getMinX() {
		return xPos - this.image.getWidth()*scale/2;
	}
	
	public float getMaxX() {
		return xPos + this.image.getWidth()*scale/2;
	}
	
	public float getMinY() {
		return yPos - this.image.getHeight()*scale/2;
	}
	
	public float getMaxY() {
		return yPos + this.image.getHeight()*scale/2;
	}
}
