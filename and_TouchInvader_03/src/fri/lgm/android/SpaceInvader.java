package fri.lgm.android;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BitmapFactory.Options;
import android.graphics.Canvas;
import android.graphics.RectF;

public class SpaceInvader extends GameObject {
	static int invaderCount = 0;
	int invaderId;
	Bitmap image = BitmapFactory.decodeResource(this.viri, R.drawable.invader1);
	Bitmap imageA = BitmapFactory.decodeResource(this.viri, R.drawable.invader1a);
	static Bitmap invader1 = null;
	static Bitmap invader2 = null;
	static Bitmap invader3 = null;
	static Bitmap invader1a = null;
	static Bitmap invader2a = null;
	static Bitmap invader3a = null;
	
	float xPos = 0.0f, yPos = 0.0f;
	int directionX = 1;
	int type = 0;
	
	public SpaceInvader(Resources res) {
		super(res);
		invaderId = invaderCount;
		invaderCount++;
	}
	
	public void render(Canvas c) {
		float xP = xPos-(image.getWidth()*scale)/2.0f;
		float yP = yPos-(image.getHeight()*scale)/2.0f;
		float xS = xPos+(image.getWidth()*scale)/2.0f;
		float yS = yPos+(image.getHeight()*scale)/2.0f;
		
		c.drawBitmap(image, null, new RectF(xP, yP, xS, yS), mPaint);
	}
	
	public void setInvaderType(int type, int sampleSize) {
		this.type = type;
		Options o = new Options();
		o.inSampleSize = sampleSize;
		switch(type) {
			case 1:
				if(invader1 == null) {
					invader1 = BitmapFactory.decodeResource(this.viri, R.drawable.invader1, o);
					invader1a = BitmapFactory.decodeResource(this.viri, R.drawable.invader1a, o);
				}
				image = invader1;
				imageA = invader1a;
				break;
			case 2:
				if(invader2 == null) {
					invader2 = BitmapFactory.decodeResource(this.viri, R.drawable.invader2, o);
					invader2a = BitmapFactory.decodeResource(this.viri, R.drawable.invader2a, o);
				}
				image = invader2;
				imageA = invader2a;
				break;
			case 3:
				if(invader3 == null) {
					invader3 = BitmapFactory.decodeResource(this.viri, R.drawable.invader3, o);
					invader3a = BitmapFactory.decodeResource(this.viri, R.drawable.invader3a, o);
				}
				image = invader3;
				imageA = invader3a;
				break;
		}
	}
	
	public void switchImage() {
		Bitmap tmp = image;
		image = imageA;
		imageA = tmp;
	}
	
	public void setPosition(float x, float y) {
		this.xPos = x;
		this.yPos = y;
	}
	
	public void moveX() {
		xPos += getWidth()/4*directionX;
	}
	
	public void moveY() {
		yPos += getHeight()/2;
	}
	
	public void changeDirectionX() {
		directionX = directionX == 1 ? -1 : 1;
		moveY();
	}
	
	public int getDirectionX() {
		return directionX;
	}
	
	public float getWidth() {
		return this.image.getWidth()*scale;
	}
	
	public float getHeight() {
		return this.image.getHeight()*scale;
	}
	
	public float getX() {
		return xPos;
	}
	
	public float getY() {
		return yPos;
	}
	
	public float getMinX() {
		return xPos - this.image.getWidth()*scale/2;
	}
	
	public float getMaxX() {
		return xPos + this.image.getWidth()*scale/2;
	}
	
	public float getMinY() {
		return yPos - this.image.getHeight()*scale/2;
	}
	
	public float getMaxY() {
		return yPos + this.image.getHeight()*scale/2;
	}
	
	public int getScore() {
		return type*10;
	}
}
