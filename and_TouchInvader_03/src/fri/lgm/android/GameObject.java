package fri.lgm.android;

import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.Paint;

public abstract class GameObject implements Renderable {
	Paint mPaint;
	Resources viri;
	float scale = 1.0f;
	
	public GameObject(Resources res) {
		mPaint = new Paint();
		this.viri = res;
	}
	
	public void setScale(float scale) {
		this.scale = scale;
	}
}
