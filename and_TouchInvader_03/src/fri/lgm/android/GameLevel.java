package fri.lgm.android;

import android.content.res.Resources;
import android.graphics.Canvas;

public class GameLevel extends GameObject {
	
	private int level;
	public GameLevel(Resources res) {
		super(res);
		this.level = 1;
	}

	public void render(Canvas c) {
		
	}
	
	public synchronized float getSpeedFactor() {
		return 1.0f/(1 + level/10.0f);
	}

	public synchronized void increment() {
		level++;
	}
	
	public synchronized int getLevel() {
		return level;
	}
}
