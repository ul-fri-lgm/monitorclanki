package fri.lgm.android;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BitmapFactory.Options;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;

public class GUI extends GameObject {
	Options o = new Options();
	private Bitmap no0; 
	private Bitmap no1; 
	private Bitmap no2;
	private Bitmap no3;
	private Bitmap no4;
	private Bitmap no5;
	private Bitmap no6;
	private Bitmap no7;
	private Bitmap no8;
	private Bitmap no9;
	
	private Bitmap scoreImage;
	private Bitmap gameOverImage;
	private Bitmap newLevelImage;
	private Bitmap livesImage;
	private Bitmap ship;
	private Bitmap leftArrow;
	private Bitmap rightArrow;
	private Bitmap title;
	private Bitmap play;
	private Bitmap exit;
	

	private Bitmap inv1;
	private Bitmap inv2;
	private Bitmap inv3;
	private Bitmap inv4;
	private Bitmap inv5;
	private Bitmap inv6;
	
	private float width, height;
	private Paint transPaint;
	
	private String score = "";
	private int lives = 0;
	private int level = 1;

	public GUI(Resources res, int sampleSize) {
		super(res);
		
		//resize pics
		Options o = new Options();
		o.inSampleSize = sampleSize;
		no0 = BitmapFactory.decodeResource(this.viri, R.drawable.no0, o);
		no1 = BitmapFactory.decodeResource(this.viri, R.drawable.no1, o);
		no2 = BitmapFactory.decodeResource(this.viri, R.drawable.no2, o);
		no3 = BitmapFactory.decodeResource(this.viri, R.drawable.no3, o);
		no4 = BitmapFactory.decodeResource(this.viri, R.drawable.no4, o);
		no5 = BitmapFactory.decodeResource(this.viri, R.drawable.no5, o);
		no6 = BitmapFactory.decodeResource(this.viri, R.drawable.no6, o);
		no7 = BitmapFactory.decodeResource(this.viri, R.drawable.no7, o);
		no8 = BitmapFactory.decodeResource(this.viri, R.drawable.no8, o);
		no9 = BitmapFactory.decodeResource(this.viri, R.drawable.no9, o);
		
		scoreImage = BitmapFactory.decodeResource(this.viri, R.drawable.score, o);
		gameOverImage = BitmapFactory.decodeResource(this.viri, R.drawable.game_over, o);
		newLevelImage = BitmapFactory.decodeResource(this.viri, R.drawable.new_level, o);
		livesImage = BitmapFactory.decodeResource(this.viri, R.drawable.lives, o);
		ship = BitmapFactory.decodeResource(this.viri, R.drawable.ship, o);
		Options o2 = new Options();
		o2.inSampleSize = sampleSize / 2;
		leftArrow = BitmapFactory.decodeResource(this.viri, R.drawable.left, o2);
		rightArrow = BitmapFactory.decodeResource(this.viri, R.drawable.right, o2);
		title = BitmapFactory.decodeResource(this.viri, R.drawable.title, o);
		play = BitmapFactory.decodeResource(this.viri, R.drawable.play, o);
		exit = BitmapFactory.decodeResource(this.viri, R.drawable.exit, o);
		

		inv1 = BitmapFactory.decodeResource(this.viri, R.drawable.invader1, o);
		inv2 = BitmapFactory.decodeResource(this.viri, R.drawable.invader1a, o);
		inv3 = BitmapFactory.decodeResource(this.viri, R.drawable.invader2, o);
		inv4 = BitmapFactory.decodeResource(this.viri, R.drawable.invader2a, o);
		inv5 = BitmapFactory.decodeResource(this.viri, R.drawable.invader3, o);
		inv6 = BitmapFactory.decodeResource(this.viri, R.drawable.invader3a, o);
		
		transPaint = new Paint();
		transPaint.setARGB(128, 0, 0, 0);
	}
	
	public void render(Canvas c) {
		switch(Game.getCurrentGameState()) {
			case Game.MAIN_MENU_STATE:
				float xPos = (float)(Math.random()*width-50);
				float yPos = (float)(Math.random()*height-50);
				float xP = xPos-(inv1.getWidth()*scale)/2.0f;
				float yP = yPos-(inv1.getHeight()*scale)/2.0f;
				float xS = xPos+(inv1.getWidth()*scale)/2.0f;
				float yS = yPos+(inv1.getHeight()*scale)/2.0f;
				c.drawBitmap(inv1, null, new RectF(xP, yP, xS, yS), mPaint);

				xPos = (float)(Math.random()*width-50);
				yPos = (float)(Math.random()*height-50);
				xP = xPos-(inv1.getWidth()*scale)/2.0f;
				yP = yPos-(inv1.getHeight()*scale)/2.0f;
				xS = xPos+(inv1.getWidth()*scale)/2.0f;
				yS = yPos+(inv1.getHeight()*scale)/2.0f;
				c.drawBitmap(inv2, null, new RectF(xP, yP, xS, yS), mPaint);

				xPos = (float)(Math.random()*width-50);
				yPos = (float)(Math.random()*height-50);
				xP = xPos-(inv1.getWidth()*scale)/2.0f;
				yP = yPos-(inv1.getHeight()*scale)/2.0f;
				xS = xPos+(inv1.getWidth()*scale)/2.0f;
				yS = yPos+(inv1.getHeight()*scale)/2.0f;
				c.drawBitmap(inv3, null, new RectF(xP, yP, xS, yS), mPaint);

				xPos = (float)(Math.random()*width-50);
				yPos = (float)(Math.random()*height-50);
				xP = xPos-(inv1.getWidth()*scale)/2.0f;
				yP = yPos-(inv1.getHeight()*scale)/2.0f;
				xS = xPos+(inv1.getWidth()*scale)/2.0f;
				yS = yPos+(inv1.getHeight()*scale)/2.0f;
				c.drawBitmap(inv4, null, new RectF(xP, yP, xS, yS), mPaint);

				xPos = (float)(Math.random()*width-50);
				yPos = (float)(Math.random()*height-50);
				xP = xPos-(inv1.getWidth()*scale)/2.0f;
				yP = yPos-(inv1.getHeight()*scale)/2.0f;
				xS = xPos+(inv1.getWidth()*scale)/2.0f;
				yS = yPos+(inv1.getHeight()*scale)/2.0f;
				c.drawBitmap(inv5, null, new RectF(xP, yP, xS, yS), mPaint);

				xPos = (float)(Math.random()*width-50);
				yPos = (float)(Math.random()*height-50);
				xP = xPos-(inv1.getWidth()*scale)/2.0f;
				yP = yPos-(inv1.getHeight()*scale)/2.0f;
				xS = xPos+(inv1.getWidth()*scale)/2.0f;
				yS = yPos+(inv1.getHeight()*scale)/2.0f;
				c.drawBitmap(inv6, null, new RectF(xP, yP, xS, yS), mPaint);

				c.drawBitmap(title, null, new RectF(width/2-title.getWidth()/4, 10, width/2+title.getWidth()/4, title.getHeight()/2+10), mPaint);
				c.drawBitmap(play, null, new RectF(width/2-play.getWidth()/4, 250, width/2+play.getWidth()/4, play.getHeight()/2+250), mPaint);
				c.drawBitmap(exit, null, new RectF(width/2-exit.getWidth()/4, 350, width/2+exit.getWidth()/4, exit.getHeight()/2+350), mPaint);
				break;
			case Game.GAMEPLAY_STATE:
				// prosojna podlaga
				c.drawRect(new RectF(0, 0, width, 25 + scoreImage.getHeight()/2), transPaint);

				// prikazovanje tock
				c.drawBitmap(scoreImage, null, new RectF(10, 20, scoreImage.getWidth()/2+10, scoreImage.getHeight()/2+20), mPaint);
				int i;
				
				String scoreLevel = score + " " + level;
				
				for (i=0; i<scoreLevel.length(); i++) {
					int x = 10 + scoreImage.getWidth()/2 + i * no0.getWidth()/2;
					int y = 20;
					int no = Integer.MIN_VALUE;
					try {
					no = Integer.parseInt(scoreLevel.substring(i, i+1));
					} catch(Exception e) { }
					switch (no) {
						case 0:
							c.drawBitmap(no0, null, new RectF(x, y, no0.getWidth()/2+x, no0.getHeight()/2+y), mPaint);
							break;
						case 1:
							c.drawBitmap(no1, null, new RectF(x, y, no0.getWidth()/2+x, no0.getHeight()/2+y), mPaint);
							break;
						case 2:
							c.drawBitmap(no2, null, new RectF(x, y, no0.getWidth()/2+x, no0.getHeight()/2+y), mPaint);
							break;
						case 3:
							c.drawBitmap(no3, null, new RectF(x, y, no0.getWidth()/2+x, no0.getHeight()/2+y), mPaint);
							break;
						case 4:
							c.drawBitmap(no4, null, new RectF(x, y, no0.getWidth()/2+x, no0.getHeight()/2+y), mPaint);
							break;
						case 5:
							c.drawBitmap(no5, null, new RectF(x, y, no0.getWidth()/2+x, no0.getHeight()/2+y), mPaint);
							break;
						case 6:
							c.drawBitmap(no6, null, new RectF(x, y, no0.getWidth()/2+x, no0.getHeight()/2+y), mPaint);
							break;
						case 7:
							c.drawBitmap(no7, null, new RectF(x, y, no0.getWidth()/2+x, no0.getHeight()/2+y), mPaint);
							break;
						case 8:
							c.drawBitmap(no8, null, new RectF(x, y, no0.getWidth()/2+x, no0.getHeight()/2+y), mPaint);
							break;
						case 9:
							c.drawBitmap(no9, null, new RectF(x, y, no0.getWidth()/2+x, no0.getHeight()/2+y), mPaint);
							break;
						default:
							break;
					};
					
					// prikazovanje zivljenj
					c.drawBitmap(livesImage, null, new RectF(width - 5 * ship.getWidth()/2 - livesImage.getWidth()/2, 20, width - 5 * ship.getWidth()/2, livesImage.getHeight()/2+20), mPaint);
					for (int i1=lives; i1>0; i1--) {
						c.drawBitmap(ship, null, new RectF(width - (5-i1) * (ship.getWidth()/2 +10), 0, width - (4-i1) * (ship.getWidth()/2 +10) -10, ship.getHeight()/2), mPaint);
					}
					
					// puscice
					c.drawBitmap(leftArrow, null, new RectF(leftArrow.getWidth()/2, height-leftArrow.getHeight()/2, leftArrow.getWidth(), height), transPaint);
					c.drawBitmap(rightArrow, null, new RectF(width-rightArrow.getWidth(), height-leftArrow.getHeight()/2, width-rightArrow.getWidth()/2, height), transPaint);
				}
				
				break;
			case Game.GAME_OVER_STATE:
				c.drawRect(new RectF(0, 0, width, height), transPaint);
				xP = width/2 - (gameOverImage.getWidth() * scale / 2);
				yP = height/2 - (gameOverImage.getHeight() * scale / 2);
				xS = width/2 + (gameOverImage.getWidth() * scale / 2);
				yS = height/2 + (gameOverImage.getHeight() * scale / 2);
				
				c.drawBitmap(gameOverImage, null, new RectF(xP, yP, xS, yS), mPaint);
				break;
			case Game.NEW_LEVEL_STATE:
				c.drawRect(new RectF(0, 0, width, height), transPaint);
				xP = width/2 - (newLevelImage.getWidth() * scale / 2);
				yP = height/2 - (newLevelImage.getHeight() * scale / 2);
				xS = width/2 + (newLevelImage.getWidth() * scale / 2);
				yS = height/2 + (newLevelImage.getHeight() * scale / 2);
				
				c.drawBitmap(newLevelImage, null, new RectF(xP, yP, xS, yS), mPaint);
				break;
		}
	}
	
	public void setSize(float w, float h) {
		width = w;
		height = h;
	}
	
	public void setScore(int score) {
		this.score = ""+score;
	}
	
	public void setLives(int lives) {
		this.lives = lives;
	}
	
	public void setLevel(int level) {
		this.level = level;
	}
	
	public float getLeftX() {
		return leftArrow.getWidth();
	}
	
	public float getRightX() {
		return width - rightArrow.getWidth();
	}
}
