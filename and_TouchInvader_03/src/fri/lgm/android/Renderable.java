package fri.lgm.android;

import android.graphics.Canvas;

public interface Renderable {

	public abstract void render(Canvas c);
}
