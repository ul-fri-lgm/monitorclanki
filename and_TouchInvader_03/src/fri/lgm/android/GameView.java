package fri.lgm.android;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;

public class GameView extends View {
	private Game game;
	private Paint mPaint;
	private float imageX, imageY;
	
	private float sizeX, sizeY;

	// Konstruktor razreda
	public GameView(Context context) {
		super(context);
		initGameView();
	}

	// Konstruktor razreda
	public GameView(Context context, AttributeSet attrs) {
		super(context, attrs);
		initGameView();
	}

	// Metoda, ki inicializira lokalne spremenljivke
    private final void initGameView() {
    	mPaint = new Paint();	// Ustvarimo novo instanco razreda Paint
    }
	
    // Metoda za izris
    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);	// Klic metode nadrazreda
        if (game != null) {
        	game.render(canvas);
        }
        if (sizeX == 0 && sizeY == 0) {
        	sizeX = this.getWidth();
        	sizeY = this.getHeight();
        	game.initializeGame();
        	
        }
    }
    
    public void setGame(Game game) {
    	this.game = game;
    }
}
