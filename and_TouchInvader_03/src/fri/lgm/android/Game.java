package fri.lgm.android;

import java.util.ArrayList;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

import android.content.Context;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.widget.Toast;

public class Game {
	// Game states:
	public static final int MAIN_MENU_STATE = 0;
	public static final int GAMEPLAY_STATE = 1;
	public static final int GAME_OVER_STATE = 2;
	public static final int HIGH_SCORES_STATE = 3;
	public static final int NEW_LEVEL_STATE = 4;

	private static int currentGameState = MAIN_MENU_STATE;
	private float scale = 1.0f;

	private int score = 0;
	private int lives = 3;

	// GameView:
	GameView gameView;

	// Game objects:
	private GameLevel gameLevel;
	private ArrayList<SpaceInvader> invaders;
	private SpaceShip ship;
	private Bullet shipBullet;
	private Bullet invaderBullet;
	private ArrayList<Bullet> bullets;
	private GUI gui;
	private int sampleSize = 1;
	private Timer invaderTimer, bulletTimer, shipMoveTimer;
	// private Timer invaderT, bulletT, shipMoveT;

	// Privatna realizacija poslusalca
	private OnTouchListener touchListener = new OnTouchListener() {

		@Override
		public boolean onTouch(View v, MotionEvent event) {
			int action = event.getAction();

			switch (action) {
			case MotionEvent.ACTION_UP:
				if (shipMoveTimer != null) {
					shipMoveTimer.cancel();
				}
				break;
			case MotionEvent.ACTION_DOWN:
				switch (currentGameState) {
				case MAIN_MENU_STATE:
					startGame();
					break;
				case GAMEPLAY_STATE:
					if (shipMoveTimer != null)
						shipMoveTimer.cancel();
					shipMoveTimer = null;

					shipMoveTimer = new Timer();
					if (event.getY() > ship.getMinY()) {
						if (event.getX() > gui.getRightX()) {
							shipMoveTimer.scheduleAtFixedRate(new TimerTask() {

								@Override
								public void run() {
									if (ship.getMaxX() < gameView.getWidth()) {
										ship.moveRight();
									}
								}
							}, 0, 25);
						} else if (event.getX() < gui.getLeftX()) {
							shipMoveTimer.scheduleAtFixedRate(new TimerTask() {

								@Override
								public void run() {
									if (ship.getMinX() > 0) {
										ship.moveLeft();
									}
								}
							}, 0, 25);
						}
					} else {
						if (shipBullet == null) {
							fireShipBullet();
						}
					}
					break;
				case NEW_LEVEL_STATE:
					prepareNextLevel();
					break;
				case GAME_OVER_STATE:
					currentGameState = MAIN_MENU_STATE;
					break;
				}
				break;
			}

			return true;
		}
	};

	public Game(GameView gameView, int height, int width, double density) {
		this.gameView = gameView;
		gameView.setFocusable(true); // Omogocimo pridobivanje fokusa
		gameView.setFocusableInTouchMode(true); // Omogocimo pridobivanje fokusa
												// v primeru dotika
		gameView.requestFocus(); // Zahtevamo fokus
		gameView.setOnTouchListener(touchListener); // Dodamo poslusalca dotika
													// na nas pogled
		gameView.setGame(this);
		
		sampleSize = (int)Math.round(density);
		Log.v("tralala", width + " " + gameView.getHeight() + " ");
		System.out.println("dimenzije: " + width + " " + height + " ");
		
		gameLevel = new GameLevel(gameView.getResources());
		invaders = new ArrayList<SpaceInvader>();
		ship = new SpaceShip(gameView.getResources(), sampleSize);
		bullets = new ArrayList<Bullet>();
		gui = new GUI(gameView.getResources(), sampleSize);

	}

	public void initializeGame() {

		currentGameState = MAIN_MENU_STATE;
		setScale(0.5f);

		// initialize GUI
		gui.setSize(gameView.getWidth(), gameView.getHeight());
		gui.setScore(score);
		gui.setLives(3);
	}

	public void prepareNextLevel() {
		gameLevel.increment();

		if (bulletTimer != null) {
			bulletTimer.cancel();
			bulletTimer = null;
		}

		if (invaderTimer != null) {
			invaderTimer.cancel();
			invaderTimer = null;
		}
		if (shipMoveTimer != null) {
			shipMoveTimer.cancel();
			shipMoveTimer = null;
		}

		gui.setLevel(gameLevel.getLevel());
		lives = 3;
		gui.setLives(lives);
		startGame();
	}

	public void startGame() {

		// initialize game parameters
		currentGameState = GAMEPLAY_STATE;
		setScale(0.5f);
		if (invaders.size() > 0) {
			invaders.clear();
		}
		// initialize invaders
		float offsetY = 25 + BitmapFactory.decodeResource(
				gameView.getResources(), R.drawable.score).getHeight() / 2;
		for (int i = 0; i < 11; i++) {
			SpaceInvader invader = new SpaceInvader(gameView.getResources());
			invader.setInvaderType(3, sampleSize);
			invader.setScale(scale);
			invader.setPosition(
					invader.getWidth() / 2
							+ (invader.getWidth() + invader.getWidth() / 8)
							* (i % 11),
					invader.getHeight() / 2
							+ (invader.getHeight() + invader.getHeight() / 8)
							* ((int) (i / 11)) + offsetY);
			invaders.add(invader);
		}
		for (int i = 0; i < 11; i++) {
			SpaceInvader invader = new SpaceInvader(gameView.getResources());
			invader.setInvaderType(2, sampleSize);
			invader.setScale(scale);
			invader.setPosition(
					invader.getWidth() / 2
							+ (invader.getWidth() + invader.getWidth() / 8)
							* (i % 11),
					invader.getHeight() / 2
							+ (invader.getHeight() + invader.getHeight() / 8)
							* ((int) (i / 11) + 1) + offsetY);
			invaders.add(invader);
		}
		for (int i = 0; i < 11; i++) {
			SpaceInvader invader = new SpaceInvader(gameView.getResources());
			invader.setInvaderType(1, sampleSize);
			invader.setScale(scale);
			invader.setPosition(
					invader.getWidth() / 2
							+ (invader.getWidth() + invader.getWidth() / 8)
							* (i % 11),
					invader.getHeight() / 2
							+ (invader.getHeight() + invader.getHeight() / 8)
							* ((int) (i / 11) + 2) + offsetY);
			invaders.add(invader);
		}
		if (invaderTimer != null) {
			invaderTimer.cancel();
			invaderTimer = null;
		}
		invaderTimer = new Timer("Invaders");
		invaderTimer.scheduleAtFixedRate(new TimerTask() {

			@Override
			public void run() {

				gameLoop();
			}
		}, 0, (int) Math.round(500 * gameLevel.getSpeedFactor()));

		// initialize ship
		ship.setScale(scale);
		ship.setPosition(gameView.getWidth() / 2 - ship.getWidth(),
				gameView.getHeight() - 3 / 2 * ship.getHeight());
		shipMoveTimer = new Timer("Ship");

		// initialize bullets
		if (bulletTimer != null) {
			bulletTimer.cancel();
			bulletTimer = null;
		}
		bulletTimer = new Timer("Bullets");
		bulletTimer.scheduleAtFixedRate(new TimerTask() {

			@Override
			public void run() {
				moveShipBullet();
				moveInvaderBullet();
			}
		}, 0, 40);
	}

	private void shipCollisionDetection() {

		if (invaderBullet == null)
			return;
		if (AABB(ship.getMinX(), ship.getMinY(), ship.getMaxX(),
				ship.getMaxY(), invaderBullet.getMinX(),
				invaderBullet.getMinY(), invaderBullet.getMaxX(),
				invaderBullet.getMaxY())) {
			lives--;
			gui.setScore(score);
			gui.setLives(lives);
			if (lives == 0) {
				currentGameState = GAME_OVER_STATE;
				lives = 3;
				gui.setLives(lives);
				score = 0;
				gui.setScore(score);
			}

			invaderBullet = null;
		}

	}

	private void invaderCollisionDetection() {
		gui.setScore(score);
		for (int i = invaders.size() - 1; i >= 0 && shipBullet != null; i--) {
			SpaceInvader si = invaders.get(i);
			if (AABB(si.getMinX(), si.getMinY(), si.getMaxX(), si.getMaxY(),
					shipBullet.getMinX(), shipBullet.getMinY(),
					shipBullet.getMaxX(), shipBullet.getMaxY())) {
				score += si.getScore();
				gui.setScore(score);

				invaders.remove(i);
				if (invaders.size() == 0) {
					currentGameState = NEW_LEVEL_STATE;

				}
				shipBullet = null;
			}
		}
	}

	private boolean AABB(float minX1, float minY1, float maxX1, float maxY1,
			float minX2, float minY2, float maxX2, float maxY2) {
		if (minX1 > maxX2 || minX2 > maxX1) {
			return false;
		}
		if (maxX1 < minX2 || maxX2 < minX1) {
			return false;
		}
		if (minY1 > maxY2 || minY2 > maxY1) {
			return false;
		}
		if (maxY1 < minY2 || maxY2 < minY1) {
			return false;
		}
		return true;
	}

	private void fireShipBullet() {
		if (shipBullet == null) {
			shipBullet = new Bullet(gameView.getResources(), 0, sampleSize);
			synchronized (shipBullet) {
				shipBullet.setScale(scale);
				shipBullet.setType(0);
				if (shipBullet != null) {
					shipBullet.setPosition(ship.getX(), ship.getY());
				}
			}
		}
	}

	private void moveShipBullet() {
		invaderCollisionDetection();
		if (shipBullet != null) {
			if (shipBullet.isValid()) {
				shipBullet.move();
			} else {
				shipBullet = null;
			}
		}
	}

	private void moveInvaderBullet() {

		shipCollisionDetection();
		if (invaderBullet != null) {

			if (invaderBullet.isValid()) {
				invaderBullet.move();
			} else {
				invaderBullet = null;
			}
		}
	}

	private void fireInvaderBullet(float posX, float posY) {
		if (invaderBullet == null) {
			invaderBullet = new Bullet(gameView.getResources(),
					gameView.getHeight(), sampleSize);
			synchronized (invaderBullet) {
				invaderBullet.setScale(scale);
				invaderBullet.setType(1);
				if (invaderBullet != null) {
					invaderBullet.setPosition(posX, posY);
				}
			}
		}
	}

	private void gameLoop() {
		if (currentGameState == GAME_OVER_STATE
				|| currentGameState == NEW_LEVEL_STATE) {
			shipBullet = null;
			invaderBullet = null;
			if (bulletTimer != null) {
				// System.out.println("A, bullet timer cancelled");
				bulletTimer.cancel();
				bulletTimer = null;
			}

			if (invaderTimer != null) {
				invaderTimer.cancel();
				invaderTimer = null;
			}
			if (shipMoveTimer != null) {
				shipMoveTimer.cancel();
				shipMoveTimer = null;
			}
			lives = 3;
			if (currentGameState == GAME_OVER_STATE)
				score = 0;
			return;
		}

		float maxX = 0, minX = Integer.MAX_VALUE;
		float maxY = 0;
		int direction = 0;
		if (invaders.size() > 0) {
			for (SpaceInvader invader : invaders) {
				invader.switchImage();
				if (invader.getX() > maxX)
					maxX = invader.getX();
				if (invader.getX() < minX)
					minX = invader.getX();
				if (invader.getY() > maxY)
					maxY = invader.getY();
			}
			maxX += invaders.get(0).getWidth();
			minX -= invaders.get(0).getWidth();
			maxY += 2 * invaders.get(0).getHeight();
			direction = invaders.get(0).getDirectionX();
			if ((maxX > gameView.getWidth() && direction == 1)
					|| (minX < 0 && direction == -1)) {
				for (SpaceInvader invader : invaders)
					invader.changeDirectionX();
			} else {
				for (int i = invaders.size() - 1; i >= 0; i--) {
					invaders.get(i).moveX();
				}
			}
			if (maxY > gameView.getHeight()
					&& currentGameState != NEW_LEVEL_STATE) {
				currentGameState = GAME_OVER_STATE;
				invaderTimer.cancel();
				invaderTimer = null;
			}

			Random r = new Random();
			if (r.nextDouble() > 0.5 && invaderBullet == null) {
				SpaceInvader invader = invaders.get(r.nextInt(invaders.size()));
				fireInvaderBullet(invader.getX(), invader.getY());
			}

		}
	}

	public void render(Canvas c) {
		switch (currentGameState) {
		case MAIN_MENU_STATE:
			gui.render(c);
			break;
		case GAMEPLAY_STATE:
			gameLevel.render(c);
			if (invaders.size() > 0)
				for (int i = invaders.size() - 1; i >= 0; i--)
					invaders.get(i).render(c);
			if (shipBullet != null) {
				shipBullet.render(c);
			}
			for (Bullet b : bullets)
				b.render(c);
			ship.render(c);
			gui.render(c);
			if (invaderBullet != null) {
				invaderBullet.render(c);
			}
			break;
		case GAME_OVER_STATE:
			if (invaderBullet != null) {
				invaderBullet = null;
				if (bulletTimer != null) {
					bulletTimer.cancel();
					bulletTimer = null;
				}
			}

			if (invaders.size() > 0)
				for (int i = invaders.size() - 1; i >= 0; i--)
					invaders.get(i).render(c);
			ship.render(c);
			for (Bullet b : bullets)
				b.render(c);
			gui.render(c);
			break;
		case NEW_LEVEL_STATE:
			// render gui
			gui.render(c);
			break;
		}
		gameView.postInvalidate();
	}

	public static int getCurrentGameState() {
		return currentGameState;
	}

	public void setScale(float scale) {
		this.scale = scale;
		this.gameLevel.setScale(scale);
		for (SpaceInvader invader : invaders)
			invader.setScale(scale);
		this.ship.setScale(scale);
		this.gui.setScale(scale);
	}

}
