package fri.lgm.android;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.RectF;
import android.graphics.BitmapFactory.Options;

public class SpaceShip extends GameObject {
	Bitmap image;
	float xPos = 0.0f, yPos = 0.0f;
	
	public SpaceShip(Resources res, int sampleSize) {
		super(res);
		Options o = new Options();
		o.inSampleSize = sampleSize;
		image = BitmapFactory.decodeResource(this.viri, R.drawable.ship, o);
	}

	public void render(Canvas c) {
		float xP = xPos-(image.getWidth()*scale)/2.0f;
		float yP = yPos-(image.getHeight()*scale)/2.0f;
		float xS = xPos+(image.getWidth()*scale)/2.0f;
		float yS = yPos+(image.getHeight()*scale)/2.0f;
		
		c.drawBitmap(image, null, new RectF(xP, yP, xS, yS), mPaint);
	}
	
	public void setPosition(float x, float y) {
		this.xPos = x;
		this.yPos = y;
	}
	
	public float[] getPosition() {
		return new float[] {xPos, yPos};
	}
	
	public float getWidth() {
		return this.image.getWidth()*scale;
	}
	
	public float getHeight() {
		return this.image.getHeight()*scale;
	}
	
	public float getX() {
		return xPos;
	}
	
	public float getY() {
		return yPos;
	}
	
	public float getMinX() {
		return xPos - this.image.getWidth()*scale/2;
	}
	
	public float getMaxX() {
		return xPos + this.image.getWidth()*scale/2;
	}
	
	public float getMinY() {
		return yPos - this.image.getHeight()*scale/2;
	}
	
	public float getMaxY() {
		return yPos + this.image.getHeight()*scale/2;
	}
	
	public void moveRight() {
		this.xPos += 10;
	}
	
	public void moveLeft() {
		this.xPos -= 10;
	}
}
